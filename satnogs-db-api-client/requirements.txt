# This is a generated file; DO NOT EDIT!
#
# Please edit 'setup.cfg' to modify top-level dependencies and run
# './contrib/refresh-requirements.sh' to regenerate this file

certifi==2023.5.7
nulltype==2.3.1
python-dateutil==2.8.2
six==1.15.0
urllib3==1.25.11
versioneer==0.28
